jsonry
----
### About
Console application to transform a message string into a JSON string of it's special contents.  In particular we are searching for 3 elements:

#### Mentions
These are substrings of the format `@handle` where `handle` represents some username.  In the JSON response the format will look like:

```json
{"mentions": ["username"]}
```

#### Emoticons
These are substrings of the format `(emoticon)` where `emoticon` represents the actual emoticon to be displayed.  In the JSON response the format will look like:

```json
{"emoticons": ["emoticon"]}
```

#### Links
These substrings are any properly formatted URL.  In the JSON response we want to capture not only the URL, but also the title of the page that URL represents, the response object will look like:

```json
{"links": [{"url": "url", "title": "title"}]}
```

### Example
A full example of a parsed string would be the following:

Input: `@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016`

Output (as a string): 

```json
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "http://www.twitter.com/jdorfman/status/430511497475670016",
      "title": "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""
    }
  ]
}
```

### Install
#### Prerequisites
1. This application was built for Python version 2.7.2, there would be definite conflicts with Python version 3+
2. You will need `pip` installed to install dependencies

#### Process
Run `pip install -r requirements.txt` in the top level directory to install all dependencies

### Usage
#### Basic Usage
Run `python main.py` which will open an event loop where you can continuously pass through strings to retrieve the JSON string of special content.

#### Basic Testing
There is no built in testing framework, but we can run some basic tests by executing `python main.py < tests.txt` where `tests.txt` is a file with each line corresponding to a message to be parsed.  When the EOF is reached the program will exit on it's own.  An example of such a file is included in the top level directory.