from parser.chat import *


class InputListener:
    def start(self):
        # initialize parser
        p = ChatParser()

        # start listening for messages on stdin
        while True:
            try:
                toParse = raw_input("Enter a string to parse: ")
                print p.parse(toParse)
            except EOFError:
                # if we catch an EOFError, for example when reading from a
                # file then just indicate that the listening is done and return
                print "\nDone listening..."
                return

if __name__ == "__main__":
    l = InputListener()
    l.start()
