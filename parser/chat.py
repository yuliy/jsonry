import json
import re
import urllib2
from urlparse import urlparse

from bs4 import BeautifulSoup

from parser.abstract import *


class ChatParser(AbstractParser):
    INDEX_MENTIONS = 'mentions'
    INDEX_EMOTICONS = 'emoticons'
    INDEX_LINKS = 'links'

    def __init__(self):
        super(ChatParser, self).__init__()

    def _findMentions(self, message):
        """Return a list of matches in the string message which represent
        "mentions".

        An example would be the string "Hey @all" which would retrieve the
        match "@all".
        """
        return re.findall("(?<![a-zA-Z])@[a-zA-Z]+", message)

    def _findEmoticons(self, message):
        """Return a list of matches in the string message which represent
        "emoticons".

        An example would be the string "Hey (all)" which would retrieve the
        match "(all)".
        """
        return re.findall("\([a-zA-Z]+\)", message)

    def _findURLs(self, message):
        """Return a list of matches in the string message which represent
        URLs.

        An example would be the string "Hey www.google.com" which would
        retrieve the match "www.google.com".

        This uses a homebrew regular expression, I did some research
        on other regular expressions for more robust URL matching but for
        the purposes of this exercise tried to build one on my own.  In
        particular this blogpost
        http://daringfireball.net/2010/07/improved_regex_for_matching_urls
        seems to have good information on a more robust solution.
        """
        # saving the old style regex (does not match things like *.co.uk):
        # "(?:http[s]?://)?(?:www.)?[a-zA-Z0-9]+\.[a-zA-Z]{2,3}[a-zA-Z0-9/]+"

        return re.findall(("(?:http[s]?://)?(?:www.)?[a-zA-Z0-9]+"
                          "\.[a-zA-Z]{2,3}[\.a-zA-Z]{0,2}[a-zA-Z0-9/]+"),
                          message)

    def _findPageTitle(self, url):
        """Return a page title given a URL, returns the empty string if the
        URL is invalid.
        """
        # parse the URL and check if the scheme (http/https) is available
        # if it is not available we need to append "http://" so that
        # we can actually open the page
        o = urlparse(url)
        if (o.scheme == ''):
            url = "http://" + url

        # retrieve the HTML from the page and parse that to retrieve the title
        html = urllib2.urlopen(url).read()
        soup = BeautifulSoup(html, "lxml")
        title = soup.html.head.title

        return title.string

    def parse(self, message):
        """Parse a given string message into a JSON string which stores
        the relevant content of the string.  Particularly we are searching
        for: mentions, emoticons and URL patterns.
        """

        # clear the internal response object
        self.response = {}

        # parse the message for mentions
        for mention in self._findMentions(message):
            # strip the "@" symbol from the mention
            handle = mention[1:]
            self.addListItemToResponse(ChatParser.INDEX_MENTIONS, handle)

        # parse the message for emoticons
        for emoticon in self._findEmoticons(message):
            # strip parenthesis from the emoticon
            handle = emoticon.strip("(").strip(")")
            self.addListItemToResponse(ChatParser.INDEX_EMOTICONS, handle)

        # parse the message for URLs and find their page titles
        for url in self._findURLs(message):
            # build out the link object (url and page title), we need to
            # catch any exceptions that might be triggered by trying to
            # retrieve a page from a bad URL
            try:
                link = {}
                link['url'] = url
                link['title'] = self._findPageTitle(url)
            except urllib2.URLError:
                print "Cannot add Link, URL was not valid."
                continue
            except ValueError:
                print "Cannot add Link, malformatted URL."
                continue

            self.addListItemToResponse(ChatParser.INDEX_LINKS, link)

        return json.dumps(self.response, indent=2, separators=(',', ': '))
