import abc


class AbstractParser(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        super(AbstractParser, self).__init__()

        # dictionary to hold the response being built
        self.response = {}

    def addListItemToResponse(self, index, item):
        """Utility function to add an element to a list in our JSON
        response.  If the index for that list does not yet exist
        then we will create it using our item
        """
        if (index in self.response):
            self.response[index].append(item)
        else:
            self.response[index] = [item]

    @abc.abstractmethod
    def parse(self, message):
        """Parse a given string message into a JSON string which stores
        the relevant content of the string.  To be implemented by all
        subclasses.
        """
        return
